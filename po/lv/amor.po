# translation of amor.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Maris Nartiss <maris.kde@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: amor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-22 00:43+0000\n"
"PO-Revision-Date: 2008-01-24 16:52+0200\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:1
#, kde-format
msgid "Don't run with scissors."
msgstr "Neskrieniet ar šķērēm."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:3
#, kde-format
msgid "Never trust car salesmen or politicians."
msgstr "Nekad neuzticieties auto tirgotājiem vai politiķiem."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:5
#, kde-format
msgid ""
"Real programmers don't comment their code. It was hard to write, it should "
"be hard to understand."
msgstr ""
"Īsti programmētāji neraksta komentārus. Programmas ir grūti rakstīt, tām ir "
"jābūt arī grūti saprotamām."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:7
#, kde-format
msgid ""
"It is much easier to suggest solutions when you know nothing about the "
"problem."
msgstr ""
"Ir daudz vieglāk ieteikt risinājumus, kad pats par problēmu neko nezina."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:9
#, kde-format
msgid "You can never have too much memory or disk space."
msgstr "Nekad nevar būt pārāk daudz atmiņas vai diska vietas."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:11
#, kde-format
msgid "The answer is 42."
msgstr "Atbilde ir 42."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:13
#, kde-format
msgid "It's not a bug. It's a misfeature."
msgstr "Tā nav kļūda. Tā ir greiza iespēja."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:15
#, kde-format
msgid "Help stamp out and abolish redundancy."
msgstr "Palīdziet atrast un iznīdēt atkārtošanos."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:17
#, kde-format
msgid ""
"To maximize a window vertically, click the maximize button with the middle "
"mouse button."
msgstr ""
"Lai maksimizētu logu vertikāli, nospiediet maksimizēšanas pogu ar peles "
"vidējo pogu."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:19
#, kde-format
msgid "You can use Alt+Tab to switch between applications."
msgstr "Varat izmanoto Alt+Tab, lai pārslēgtos starp logiem."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:21
#, kde-format
msgid ""
"Press Ctrl+Esc to show the applications running in your current session."
msgstr "Ctrl+Esc parāda aktīvajā sesijā darbojošās programmas."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:23
#, kde-format
msgid "Alt+F2 displays a small window that you can type a command into."
msgstr "Alt+F2 parāda komandu palaišanas logu."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:25
#, kde-format
msgid "Ctrl+F1 to Ctrl+F8 can be used to switch virtual desktops."
msgstr "Ar Ctrl+F1 līdz Ctrl+F8 var pārslēgt virtuālās darbvirsmas."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:27
#, kde-format
msgid "You can move buttons on the panel using the middle mouse button."
msgstr "Jūs varat pārvietot paneļa pogas, izmantojot peles vidējo pogu."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:29
#, kde-format
msgid "Alt+F1 pops-up the system menu."
msgstr "Ar Alt+F1 var atvērt sistēmas izvēlni."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:31
#, kde-format
msgid ""
"Ctrl+Alt+Esc can be used to kill an application that has stopped responding."
msgstr ""
"Ar Ctrl+Alt+Esc var nokaut programmu, ja tā vairs nereaģē uz jūsu darbībām."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:33
#, kde-format
msgid ""
"If you leave KDE applications open when you logout, they will be restarted "
"automatically when you log back in."
msgstr ""
"Ja atslēdzoties esat atstājis KDE programmas atvērtas, tās tiks automātiski "
"palaistas no jauna, kad nākamreiz pieslēgsieties."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:35
#, fuzzy, kde-format
#| msgid "The KDE file manager is also a web browser and an FTP client."
msgid "The KDE file manager (Dolphin) is also an FTP client."
msgstr "KDE failu pārvaldnieks ir arī tīmekļa pārlūks un FTP klients."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:37
#, fuzzy, kde-format
#| msgid ""
#| "Applications can display messages and tips in an Amor bubble using the "
#| "showMessage() and\n"
#| "showTip() DCOP calls"
msgid ""
"Applications can display messages and tips in an Amor bubble using the "
"showMessage() and\n"
"showTip() D-Bus calls."
msgstr ""
"Programmas var izmantot Amor ziņojumu rādīšani ar showMessage() un\n"
"showTip() DCOP izsaukumiem"

#: src/amor.cpp:255
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Error reading theme: "
msgctxt "@info:status"
msgid "Error reading theme: %1"
msgstr "Kļūda nolasot tēmu: "

#: src/amor.cpp:265 src/amor.cpp:272
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Error reading group: "
msgctxt "@info:status"
msgid "Error reading group: %1"
msgstr "Kļūda nolasot grupu: "

#: src/amor.cpp:518
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Help"
msgstr "&Palīdzība"

#. i18n'ed
#: src/amor.cpp:522
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Configure..."
msgstr "&Konfigurēt..."

#: src/amor.cpp:525
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Quit"
msgstr "&Iziet"

#: src/amordialog.cpp:42
#, fuzzy, kde-format
#| msgid "Options"
msgctxt "@title:window"
msgid "Options"
msgstr "Opcijas"

#: src/amordialog.cpp:65
#, kde-format
msgid "Theme:"
msgstr "Tēma:"

#: src/amordialog.cpp:82
#, kde-format
msgid "Offset:"
msgstr "Nobīde:"

#: src/amordialog.cpp:93
#, kde-format
msgid "Always on top"
msgstr "Palikt virspusē"

#: src/amordialog.cpp:98
#, kde-format
msgid "Show random tips"
msgstr "Rādīt gadījuma padomus"

#: src/amordialog.cpp:103
#, kde-format
msgid "Use a random character"
msgstr "Lietot gadījuma radījumu"

#: src/amordialog.cpp:108
#, kde-format
msgid "Allow application tips"
msgstr "Lietot programmas padomus"

#: src/main.cpp:28
#, kde-format
msgid "KDE creature for your desktop"
msgstr "Dzīvnieciņš jūsu KDE darbvirsmai"

#: src/main.cpp:38
#, kde-format
msgid "amor"
msgstr "amor"

#: src/main.cpp:41
#, kde-format
msgid ""
"1999 by Martin R. Jones\n"
"2010 by Stefan Böhmann"
msgstr ""

#: src/main.cpp:44
#, kde-format
msgid "Stefan Böhmann"
msgstr ""

#: src/main.cpp:45
#, kde-format
msgid "Current maintainer"
msgstr "Pašreizējais uzturētājs"

#: src/main.cpp:51
#, kde-format
msgid "Martin R. Jones"
msgstr "Martin R. Jones"

#: src/main.cpp:52
#, kde-format
msgid "Gerardo Puga"
msgstr "Gerardo Puga"

#~ msgid "No tip"
#~ msgstr "Nav padomu"

#~ msgid "Copyright 1999, Martin R. Jones"
#~ msgstr "Autortiesības 1999, Martin R. Jones"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Amor Version %1\n"
#~ "\n"
#~ msgstr ""
#~ "Amor versija %1\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Amusing Misuse Of Resources\n"
#~ "\n"
#~ msgstr ""
#~ "Amizanta Resursu Šķērdēšana\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Copyright 1999 Martin R. Jones <email>mjones@kde.org</email>\n"
#~ "\n"
#~ msgstr ""
#~ "Autortiesības 1999 Martin R. Jones <email>mjones@kde.org</email>\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid "Original Author: Martin R. Jones <email>mjones@kde.org</email>\n"
#~ msgstr "Orģinālais autors: Martin R. Jones <email>mjones@kde.org</email>\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Current Maintainer: Gerardo Puga <email>gpuga@gioia.ing.unlp.edu.ar</"
#~ "email>\n"
#~ msgstr ""
#~ "Šobrīdējais uzturētājs: Gerardo Puga <email>gpuga@gioia.ing.unlp.edu.ar</"
#~ "email>\n"

#~ msgctxt "@label:textbox"
#~ msgid "About Amor"
#~ msgstr "Par Amor"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Viesturs Zariņš"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "viesturs.zarins@mi.lu.lv"
